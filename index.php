<?php

require('animal.php');
require('ape.php');
require('frog.php');

$sheep = new animal("shaun");

echo "Name : " . $sheep->names . "<br>"; // "shaun"
echo "legs : " . $sheep->legs . "<br>"; // 4
echo "cold blooded  :  $sheep->cold_blooded <br><br>"; // "no"

$sungokong  = new Ape("kera sakti");
echo "Name : " . $sungokong->names . "<br>"; 
echo "legs : " . $sungokong->legs . "<br>"; 
echo "cold blooded : " . $sungokong->cold_blooded . "<br>"; 
$sungokong->yell();

$kodok = new frog("buduk");
echo "<br><br> Name : " . $kodok->names . "<br>"; 
echo "legs: " . $kodok->legs . "<br>"; 
echo "cold blooded : " . $kodok->cold_blooded . "<br>"; 
$kodok->jump() ; // "hop hop"
?>

